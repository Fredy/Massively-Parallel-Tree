# Massively-Parallel-Tree
Hilbert R-Tree based on [Hilbert R-Tree: An improved R-Tree Using Fractals](http://www.vldb.org/conf/1994/P500.PDF)

Hilbert value implementation based on [Compact Hilbert Indices for Multi-Dimensional Data](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.649.2193&rep=rep1&type=pdf) and [Compact Hilbert Indices](https://www.cs.dal.ca/research/techreports/cs-2006-07).

Build parallelization based on [Exploiting Massive Parallelism for IndexingMulti-Dimensional Datasets on the GPU](http://ieeexplore.ieee.org/document/6876171/). This also has a search algorithm: MPRS
