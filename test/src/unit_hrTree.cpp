#include "catch.hpp"
#include "node/leafEntry.hpp"
#include "node/node.hpp"
#include "node/nonLeafEntry.hpp"
#include "rectangle/rectangle.hpp"
#include "tree/hrtree.hpp"
using tree::HRtree;
class TestHRtree : public HRtree {
public:
  using HRtree::assignLeafsHilbertVal;
  using HRtree::bottomUpBuild;
  using HRtree::calcNodeLevels;
  using HRtree::createLeafEntries;
  using HRtree::moveLeafEntriesToNode;
  using HRtree::pointToPointULL;
};

TEST_CASE("HR Tree") {
  TestHRtree tree;
  std::vector<Point> points(4);
  for (size_t i = 0; i < points.size(); i++) {
    points[i] = {1.0 * i, 2.0 * i, 3.0 * i};
  }

  SECTION("Create leaf entries") {
    std::vector<node::LeafEntry> leaf = tree.createLeafEntries(points);
    REQUIRE(leaf[0].getIndex() == 0);
    REQUIRE(leaf[1].getIndex() == 1);
    REQUIRE(leaf[2].getIndex() == 2);
    REQUIRE(leaf[3].getIndex() == 3);

    REQUIRE(leaf[0].getPoint() == points[0]);
    REQUIRE(leaf[1].getPoint() == points[1]);
    REQUIRE(leaf[2].getPoint() == points[2]);
    REQUIRE(leaf[3].getPoint() == points[3]);
  }

  SECTION("Point to ULLPoint") {
    PointULL pointULL = tree.pointToPointULL(points[1], points[1].size());
    REQUIRE(pointULL[0] == size_t(1.0 * 1e6) + size_t(1e11));
    REQUIRE(pointULL[1] == size_t(2.0 * 1e6) + size_t(1e11));
    REQUIRE(pointULL[2] == size_t(3.0 * 1e6) + size_t(1e11));
  }

  SECTION("Assigning Hilbert values and deleting it") {
    std::vector<node::LeafEntry> leafs = tree.createLeafEntries(points);
    tree.assignLeafsHilbertVal(leafs);

    SECTION("Assigning Hilbert values") {
      // Use count == 2 because one is inside the node and other is the one we
      // create when getHilbertValue is called.
      REQUIRE(leafs[0].getHilbertValue().use_count() == 2);
      REQUIRE(leafs[1].getHilbertValue().use_count() == 2);
      REQUIRE(leafs[2].getHilbertValue().use_count() == 2);
      REQUIRE(leafs[3].getHilbertValue().use_count() == 2);
    }
    SECTION("Deleting Hilbert Values") {
      for (size_t i = 0; i < leafs.size(); i++) {
        leafs[i].deleteHilbertValue();
      }
      REQUIRE(leafs[0].getHilbertValue().use_count() == 0);
      REQUIRE(leafs[1].getHilbertValue().use_count() == 0);
      REQUIRE(leafs[2].getHilbertValue().use_count() == 0);
      REQUIRE(leafs[3].getHilbertValue().use_count() == 0);
    }
  }

  SECTION("Move Leaf entries to nodes") {
    std::vector<node::LeafEntry> leafEntries(1980);
    for (size_t i = 0; i < leafEntries.size(); i++) {
      leafEntries[i].setPoint(Point(1, i));
      leafEntries[i].setIndex(i);
    }
    std::vector<size_t> nodeLvlsCnt = tree.calcNodeLevels(leafEntries.size());
    size_t totalNodeCnt = 0;
    for (const auto &n : nodeLvlsCnt)
      totalNodeCnt += n;
    size_t leafNodeCnt = nodeLvlsCnt.back();

    std::vector<std::shared_ptr<node::Node>> tmpNodes =
        tree.moveLeafEntriesToNode(leafEntries, leafNodeCnt);

    REQUIRE(tmpNodes.size() == 11);
    REQUIRE(tmpNodes.front()->getEntries().size() == 192);
    REQUIRE(tmpNodes.back()->getEntries().size() == 60);
    REQUIRE(tmpNodes.back()->getEntries().back()->getIndex() == 1979);

    SECTION("Bottom up") {
      auto node = tree.bottomUpBuild(tmpNodes, nodeLvlsCnt);
      REQUIRE(node->getEntries().size() == nodeLvlsCnt[1]);
      auto leafEntry = std::static_pointer_cast<node::NonLeafEntry>(
                           node->getEntries().front())
                           ->getNode()
                           ->getEntries()
                           .front();
      REQUIRE(leafEntry->isLeafEntry() == true);
      REQUIRE(
          std::static_pointer_cast<node::LeafEntry>(leafEntry)->getPoint() ==
          Point{0});
      REQUIRE(node->getEntries().back()->getIndex() == 1979);

      auto mbr = node->getMBR();
      REQUIRE(mbr->getLower() == Point{0});
      REQUIRE(mbr->getUpper() == Point{1979});
    }
  }

  SECTION("Verify the MBR of leaf nodes") {
    std::vector<node::LeafEntry> leafEntries(5);

    for (size_t i = 0; i < leafEntries.size(); i++) {
      leafEntries[i].setIndex(i);
    }
    leafEntries[0].setPoint({0, 1, 2, 3});
    leafEntries[1].setPoint({2, 2, 9, 1});
    leafEntries[2].setPoint({3, 4, 3, 3});
    leafEntries[3].setPoint({4, 1, 8, 7});
    leafEntries[4].setPoint({5, 0, 2, 8});

    std::vector<size_t> nodeLvlsCnt = tree.calcNodeLevels(leafEntries.size());
    size_t totalNodeCnt = 0;
    for (const auto &n : nodeLvlsCnt)
      totalNodeCnt += n;
    size_t leafNodeCnt = nodeLvlsCnt.back();

    std::vector<std::shared_ptr<node::Node>> tmpNodes =
        tree.moveLeafEntriesToNode(leafEntries, leafNodeCnt);

    REQUIRE(tmpNodes.size() == 1);
    auto rect = tmpNodes.front()->getMBR();
    REQUIRE(rect->getUpper() == Point{5, 4, 9, 8});
    REQUIRE(rect->getLower() == Point{0, 0, 2, 1});
  }
}
