#pragma once

#include "other/types.hpp"
#include "other/config.hpp"
#include <boost/dynamic_bitset.hpp>

namespace helpers {
namespace hilbert {

static const size_t DIM = config::PointDimension();

bitset2<DIM> oneBitForEachDim(const PointULL &point,
                              const size_t bitPos) {
  assert(bitPos <= sizeof(size_t) * 8);

  const size_t mask = 1ull << bitPos;

  bitset2<DIM> res;

  for (size_t i = DIM; i-- > 0;) { // NOTE: 'i' starts at dimN - 1;
    if (mask & point[i])
      res.set(DIM - i - 1);
  }
  return res;
}

template<size_t size>
void grayCodeInverse(bitset2<size> &bitset) {
  bitset2<size> mask = bitset >> 1;
  for (; !mask.none(); mask >>= 1)
    bitset ^= mask;
}

template<size_t size>
void grayCode(bitset2<size> &bitset) { bitset ^= (bitset >> 1); }

template<size_t size>
size_t findNextClear(const bitset2<size> &bs, size_t index) {
  for (size_t i = index + 1; i < size; i++) {
    if (!bs.test(i)) {
      return i;
    }

  }
  return bitset2<size>::npos;
}

template<size_t size>
size_t lowestDiffBit(const bitset2<size> &bitset) {
  size_t val;
  if (bitset.none())
    val = 0;
  else {
    if (bitset.test(0)) {
      //bitset2<size> tmp = bitset;
      //size_t tsb = tmp.flip().find_next(0);
      size_t tsb = findNextClear(bitset, 0);
      assert(tsb <= bitset.size() | tsb == bitset2<size>::npos);

      val = (tsb == bitset2<size>::npos) ? 0 : tsb;
    } else {
      size_t tcb = bitset.find_next(0);
      assert(0 < tcb and tcb < bitset.size());

      val = tcb;
    }

  }

  assert(val == 0 || (0 < val and val < bitset.size()));
  return val;
}
///////
template<size_t size>
void smallerEvenAndGrayCode(bitset2<size> &bitset) {
  if (bitset.test(0)) {
    bitset.set(0, false);
    grayCode(bitset);
  } else {
    size_t firstSetIndex = bitset.find_next(0);
    if (firstSetIndex != bitset2<size>::npos) {
      bitset.reset(firstSetIndex);
      for (size_t i = 1; i < firstSetIndex; i++)
        bitset.set(i);

      assert(!bitset.test(0));
      grayCode(bitset);
    }
  }
}

template<size_t size>
size_t updateD(size_t d, const bitset2<size> &bitset) {
  d += lowestDiffBit(bitset) + 1;
  d %= bitset.size();
  return d;
}

template<size_t size>
bitset2<size> updateE(size_t d, bitset2<size> w,
                      const bitset2<size> &e) {
  smallerEvenAndGrayCode(w);
  w.rotate_left(d);
  return e ^ w;
}

} // namespace hilbert
} // namespace helpers
