#pragma once

#include <vector>
#include "bitset2/bitset2.hpp"
#include "config.hpp"

template<size_t size>
using bitset2 = Bitset2::bitset2<size>;

using HilbertValue = bitset2<config::HilbertValPrecision() * config::PointDimension()>;

using std::size_t;
using Point = std::vector<double>;
using PointULL = std::vector<size_t>;
using ui = unsigned int;
using ul = unsigned long;
using ull = unsigned long long;
using ll = long long;
