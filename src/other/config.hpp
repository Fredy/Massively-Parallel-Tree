#pragma once

#include "types.hpp"

namespace config {

// TODO: make this functions available in device and host
// TODO: add bit precision.

constexpr size_t PointDimension() { return 90; }

constexpr const size_t MaxLeafEntries() { return 192; }

constexpr const size_t MaxNonLeafEntries() { return 128; }

constexpr const size_t HilbertValPrecision() { return 40; }

} // namespace config