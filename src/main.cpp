#include "other/files.hpp"
#include "other/mtime.hpp"
#include "tree/hrtree.hpp"
#include "node/leafEntry.hpp"
#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>
#include <vector>
class TestHRtree : public tree::HRtree {
public:
  using tree::HRtree::assignLeafsHilbertVal;
  using tree::HRtree::bottomUpBuild;
  using tree::HRtree::calcNodeLevels;
  using tree::HRtree::createLeafEntries;
  using tree::HRtree::moveLeafEntriesToNode;
  using tree::HRtree::pointToPointULL;
  using tree::HRtree::removeHilbertValues;
};

void printTime(const std::string msg, double time) {
  std::cout << std::fixed << std::setprecision(4);
  std::cout << msg << ": " << time << " ms.\n";
}

int main() {
  std::fstream file("dataM.csv");
  if (!file.is_open()) {
    std::cout << "No file\n";
    return 0;
  }
  std::cout << "Start!\n";
  std::vector<Point> pointsORG;

  auto time = mtime::mTime([&] { pointsORG = files::readCSV(file); });

  printTime("READ TIME", time / 1000.0);
  // std::cout << "Points readed\n";

  // Remove repeated points.
  std::sort(pointsORG.begin(), pointsORG.end());
  pointsORG.erase(std::unique(pointsORG.begin(), pointsORG.end()), pointsORG.end());


  // 515,345
  // const size_t size = 400 * 1000;
  std::cout << "POINTS: " << pointsORG.size() << '\n';
  std::cout << "DIMENSIONS: " << pointsORG.at(0).size() << '\n';
  //pointsORG.resize(size);

  // std::cout << "Remove repeated points\n";

/*
  tree::HRtree tree;
  {
    TestHRtree treeTst;
    std::vector<node::LeafEntry> leafs;
    time = mtime::mTime([&] { leafs = treeTst.createLeafEntries(pointsORG); });

    printTime("CREATE LEAFS TIME", time / 1000.0);
    time = mtime::mTime([&] { treeTst.assignLeafsHilbertVal(leafs); });
    printTime("ASSIGN HILBERT VAL", time / 1000.0);

    time = mtime::mTime([&] { treeTst.removeHilbertValues(leafs); });
    printTime("REMOVE HILBERT VAL", time / 1000.0);
  }
*/
{
  tree::HRtree tree;
  time = mtime::mTime([&] { tree.build(pointsORG); });
}
  printTime("BUILD TIME FULL", time / 1000.0);
{
  tree::HRtree tree;
  std::vector<Point> pointsCPY(pointsORG.begin(), pointsORG.begin() + 400000);
  time = mtime::mTime([&] { tree.build(pointsCPY); });

}
  printTime("BUILD TIME 400k", time / 1000.0);
{
  tree::HRtree tree;
  std::vector<Point> pointsCPY(pointsORG.begin(), pointsORG.begin() + 300000);
  time = mtime::mTime([&] { tree.build(pointsCPY); });

}
  printTime("BUILD TIME 300k", time / 1000.0);
{
  tree::HRtree tree;
  std::vector<Point> pointsCPY(pointsORG.begin(), pointsORG.begin() + 200000);
  time = mtime::mTime([&] { tree.build(pointsCPY); });

}
  printTime("BUILD TIME 200k", time / 1000.0);
{
  tree::HRtree tree;
  std::vector<Point> pointsCPY(pointsORG.begin(), pointsORG.begin() + 100000);
  time = mtime::mTime([&] { tree.build(pointsCPY); });

}
  printTime("BUILD TIME 100K", time / 1000.0);



  std::cout << "Done\n";

  return 0;
}
