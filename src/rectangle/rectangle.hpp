#pragma once

#include "other/types.hpp"

namespace rectangle {

class Rectangle {
private:
  //! Stores the max values of the dimension.
  Point upper;
  //! Stores the min values of the dimension.
  Point lower;

public:
  Rectangle() = default;
  ~Rectangle() = default;
  Rectangle(const Point &lower, const Point &upper);

  /**
   * @brief Check if this rectangle intersect with the rectangle passed as
   * parameter.
   */
  bool intersects(const Rectangle &rct) const;

  /**
   * @brief Check if this rectangle contains the rectangle passed as parameter.
   */
  bool contains(const Rectangle &rct) const;

  /**
   * @brief Check if this rectangle contains the point passed as parameter.
   */
  bool contains(const Point &point) const;

  const Point &getUpper() const;
  const Point &getLower() const;
};

} // namespace rectangle
