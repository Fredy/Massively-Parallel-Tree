#include "rectangle.hpp"
#include <cassert>

namespace rectangle {

const Point &Rectangle::getUpper() const { return this->upper; }

const Point &Rectangle::getLower() const { return this->lower; }

Rectangle::Rectangle(const Point &lower, const Point &upper) {
  this->lower = lower;
  this->upper = upper;
}

// TODO: implement intersects and contains rectangle. Are these necessary ?
bool Rectangle::intersects(const Rectangle &rct) const { return false; }

bool Rectangle::contains(const Rectangle &rct) const { return false; }

bool Rectangle::contains(const Point &point) const {
  assert(this->lower.size() == point.size());
  const size_t dims = this->lower.size();

  for (size_t i = 0; i < dims; i++) {
    // ! ( Lower <= point <= upper )
    if (lower[i] > point[i] or upper[i] < point[i])
      return false;
  }
  return true;
}

} // namespace rectangle
