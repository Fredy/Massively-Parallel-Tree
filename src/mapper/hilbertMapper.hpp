#pragma once
#include "helpers/hilbertHelper.hpp"
#include "other/types.hpp"
#include <boost/dynamic_bitset.hpp>

namespace mapper {

static const size_t PREC = config::HilbertValPrecision();
static const size_t DIM = config::PointDimension();

// Input: n, m ∈ Z + and a point p ∈ P.
/**
 * @brief Converts a point consisting of unsigned long longs to its
 * hilbert value.
 * @param point An unsigned long long point.
 * @param dim Number of dimensions.
 * @param prec Bits precision.
 * @return
 */
bitset2<DIM * PREC> toHilbertValue(const PointULL &point, const size_t dim,
                                   const size_t prec) {
  bitset2<DIM * PREC> h, ones;
  bitset2<DIM> e;
  size_t d = 0;

  for (size_t i = 0; i < DIM; i++) {
    ones.set(i);
  }

  for (size_t i = prec - 1; i < prec; i--) {
    auto l = helpers::hilbert::oneBitForEachDim(point, i);

    auto w = Bitset2::rotate_right(l ^ e, d);
    helpers::hilbert::grayCodeInverse(w);

    bitset2<DIM> wRz = w;
    //wRz.resize(dim * prec);

    h = (h << dim) | Bitset2::convert_to<DIM * PREC, unsigned long long>(wRz);
    e = helpers::hilbert::updateE(d, w, e);
    d = helpers::hilbert::updateD(d, w);
  }
  return h;
}

} // namespace mapper
