#include "node.hpp"
#include "leafEntry.hpp"
#include "nodeEntry.hpp"
#include "nonLeafEntry.hpp"
#include "rectangle/rectangle.hpp"
#include <cassert>

namespace node {

Node::Node(bool leaf, const std::vector<std::shared_ptr<NodeEntry>> &entries) {
  this->leaf = leaf;
  this->entries = entries;
  this->largestIndex = this->entries.back()->getIndex();
  this->minBoundRect = this->calculateMBR();
}

Node::Node(bool leaf, std::vector<std::shared_ptr<NodeEntry>> &&entries) {
  this->leaf = leaf;
  this->entries = std::move(entries);
  this->largestIndex = this->entries.back()->getIndex();
  this->minBoundRect = this->calculateMBR();
}

const std::shared_ptr<rectangle::Rectangle> Node::getMBR() const {
  return this->minBoundRect;
}

size_t Node::getLargestIndex() const { return this->largestIndex; }

bool Node::isLeaf() { return this->leaf; }

const std::vector<std::shared_ptr<NodeEntry>> &Node::getEntries() const {
  return this->entries;
}

void Node::setEntries(const std::vector<std::shared_ptr<NodeEntry>> &entries) {
  this->entries = entries;
  this->largestIndex = this->entries.back()->getIndex();
  this->minBoundRect = this->calculateMBR();
}

void Node::setEntries(std::vector<std::shared_ptr<NodeEntry>> &&entries) {
  this->entries = std::move(entries);
  this->largestIndex = this->entries.back()->getIndex();
  this->minBoundRect = this->calculateMBR();
}

Node::Node(bool leaf) { this->leaf = leaf; }

void Node::setMBR(const std::shared_ptr<rectangle::Rectangle> &minBoundRect) {
  this->minBoundRect = minBoundRect;
}

std::shared_ptr<rectangle::Rectangle> Node::calculateMBR() {
  assert(!this->entries.empty());
  Point lower, upper;

  if (this->leaf) {
    lower = upper =
        std::static_pointer_cast<LeafEntry>(this->entries[0])->getPoint();
    for (size_t i = 1; i < this->entries.size(); i++) {
      Point pointTmp =
          std::static_pointer_cast<LeafEntry>(this->entries[i])->getPoint();
      for (size_t j = 0; j < pointTmp.size(); j++) {
        if (lower[j] > pointTmp[j])
          lower[j] = pointTmp[j];
        if (upper[j] < pointTmp[j])
          upper[j] = pointTmp[j];
      }
    }

  } else {
    std::shared_ptr<rectangle::Rectangle> tmpRectPtr =
        std::static_pointer_cast<NonLeafEntry>(this->entries[0])->getMBR();
    lower = tmpRectPtr->getLower();
    upper = tmpRectPtr->getUpper();
    for (size_t i = 1; i < this->entries.size(); i++) {
      tmpRectPtr =
          std::static_pointer_cast<NonLeafEntry>(this->entries[i])->getMBR();
      Point tmpLower = tmpRectPtr->getLower();
      Point tmpUpper = tmpRectPtr->getUpper();
      for (size_t j = 0; j < lower.size(); j++) {
        if (lower[j] > tmpLower[j])
          lower[j] = tmpLower[j];
        if (upper[j] < tmpUpper[j])
          upper[j] = tmpUpper[j];
      }
    }
  }
  return std::make_shared<rectangle::Rectangle>(lower, upper);
}

} // namespace node
