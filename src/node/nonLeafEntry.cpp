#include "nonLeafEntry.hpp"
#include "node.hpp"
#include <cassert>

namespace node {

NonLeafEntry::NonLeafEntry(const std::shared_ptr<Node> &node) {
  this->node = node;
}

const std::shared_ptr<Node> NonLeafEntry::getNode() const { return this->node; }

const std::shared_ptr<rectangle::Rectangle> NonLeafEntry::getMBR() const {
  assert(this->node);
  return this->node->getMBR();
}

size_t NonLeafEntry::getIndex() const {
  assert(this->node);
  return this->node->getLargestIndex();
}

bool NonLeafEntry::isLeafEntry() { return false; }

void NonLeafEntry::setMBR(const std::shared_ptr<rectangle::Rectangle> &minBoundRect) {
  assert(this->node);
  this->node->setMBR(minBoundRect);
}
} // namespace node
