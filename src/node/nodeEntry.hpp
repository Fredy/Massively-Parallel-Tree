#pragma once

#include "other/types.hpp"

namespace node {

class NodeEntry {
protected:
  size_t index;

public:
  virtual ~NodeEntry() = default;
  virtual size_t getIndex() const;
  virtual void setIndex(size_t index);
  virtual bool isLeafEntry() = 0;
};

} // namespace node
