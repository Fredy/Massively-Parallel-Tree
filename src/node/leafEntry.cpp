#include "leafEntry.hpp"

namespace node {

LeafEntry::LeafEntry(const Point &point) { this->point = point; }

LeafEntry::LeafEntry(LeafEntry &&other) noexcept
    : hilbertValue(std::move(other.hilbertValue)),
      point(std::move(other.point)) {
  this->index = other.index;
}

LeafEntry::LeafEntry(const LeafEntry &other)
    : hilbertValue(other.hilbertValue), point(other.point) {
  this->index = other.index;
}

const std::shared_ptr<HilbertValue>
LeafEntry::getHilbertValue() const {
  return this->hilbertValue;
}

void LeafEntry::setHilbertValue(
    const std::shared_ptr<HilbertValue> &hilbertValue) {
  this->hilbertValue = hilbertValue;
}

const Point &LeafEntry::getPoint() const { return point; }

void LeafEntry::setPoint(const Point &point) { this->point = point; }

bool LeafEntry::isLeafEntry() { return true; }

void LeafEntry::deleteHilbertValue() { this->hilbertValue.reset(); }

} // namespace node