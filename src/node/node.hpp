#pragma once

#include "nodeEntry.hpp"
#include "other/config.hpp"
#include "other/types.hpp"
#include <memory>
#include <vector>

namespace rectangle {
class Rectangle;
}

namespace node {

class Node {
private:
  std::shared_ptr<rectangle::Rectangle> minBoundRect;
  size_t largestIndex;
  std::vector<std::shared_ptr<NodeEntry>> entries;
  bool leaf;

public:
  // TODO: use MaxLeaftEntries and MaxNonLeafEntries.
  Node(bool leaf, const std::vector<std::shared_ptr<NodeEntry>> &entries);
  Node(bool leaf, std::vector<std::shared_ptr<NodeEntry>> &&entries);
  Node(bool leaf);

  bool isLeaf();

  void setMBR(const std::shared_ptr<rectangle::Rectangle> &minBoundRect);
  const std::shared_ptr<rectangle::Rectangle> getMBR() const;

  std::shared_ptr<rectangle::Rectangle> calculateMBR();

  // The largest index must be set in constructor and setEntries.
  size_t getLargestIndex() const;

  const std::vector<std::shared_ptr<NodeEntry>> &getEntries() const;
  void setEntries(const std::vector<std::shared_ptr<NodeEntry>> &entries);
  void setEntries(std::vector<std::shared_ptr<NodeEntry>> &&entries);
};

} // namespace node
