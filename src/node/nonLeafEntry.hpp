#pragma once

#include <memory>
#include "nodeEntry.hpp"

namespace rectangle {
class Rectangle;
}

namespace node {

class Node;

class NonLeafEntry : public NodeEntry {
private:
  std::shared_ptr<Node> node;

public:
  NonLeafEntry(const std::shared_ptr<Node> &node);
  const std::shared_ptr<Node> getNode() const;

  const std::shared_ptr<rectangle::Rectangle> getMBR() const;
  void setMBR(const std::shared_ptr<rectangle::Rectangle> &minBoundRect);
  size_t getIndex() const override;

  bool isLeafEntry() override;
};

} // namespace node
