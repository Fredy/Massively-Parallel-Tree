#include "nodeEntry.hpp"

namespace node {

size_t node::NodeEntry::getIndex() const { return this->index; }

void node::NodeEntry::setIndex(size_t index) { this->index = index; }

} // namespace node
