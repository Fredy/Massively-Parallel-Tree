#pragma once

#include "nodeEntry.hpp"
#include "other/types.hpp"
#include <boost/dynamic_bitset_fwd.hpp>
#include <memory>

namespace node {
class LeafEntry : public NodeEntry {
private:
  std::shared_ptr<HilbertValue> hilbertValue;
  Point point;

public:
  LeafEntry() = default;

  //! Move constructor
  LeafEntry(LeafEntry &&other) noexcept;
  LeafEntry(const LeafEntry &other);
  LeafEntry &operator=(const LeafEntry &other) = default;

  LeafEntry(const Point &point);
  const std::shared_ptr<HilbertValue> getHilbertValue() const;
  void deleteHilbertValue();
  void
  setHilbertValue(const std::shared_ptr<HilbertValue> &hilbertValue);
  const Point &getPoint() const;
  void setPoint(const Point &point);
  bool isLeafEntry() override;
};
} // namespace node
