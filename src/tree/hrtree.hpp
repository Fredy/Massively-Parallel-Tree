#pragma once

#include "other/types.hpp"
#include <memory>
#include <vector>

namespace node {
class Node;
class LeafEntry;
} // namespace node

namespace tree {
class HRtree {
protected:
  std::shared_ptr<node::Node> root;

  std::vector<node::LeafEntry>
  createLeafEntries(const std::vector<Point> &points);
  void thr_createLeafEntries(const std::vector<Point> &points,
                             std::vector<node::LeafEntry> &leafs,
                             const size_t start, const size_t end);

  void assignLeafsHilbertVal(std::vector<node::LeafEntry> &leafs);
  void thr_assignLeafHilbertVal(std::vector<node::LeafEntry> &leafs,
                                const size_t dimensions, const size_t start,
                                const size_t end);

  // TODO: send this to another class / file.
  PointULL pointToPointULL(const Point &point, const size_t dimensions);
  std::vector<size_t> calcNodeLevels(size_t numPoints);

  // TODO: make it a const ref?
  void bottomUp(std::vector<node::LeafEntry> &leafs);

  std::vector<std::shared_ptr<node::Node>>
  moveLeafEntriesToNode(std::vector<node::LeafEntry> &leafs,
                        const size_t leafCnt);
  void
  thr_moveLeafEntriesToNode(std::vector<node::LeafEntry> &leafs,
                            std::vector<std::shared_ptr<node::Node>> &nodes,
                            const size_t leafsStart, const size_t leafsEnd,
                            const size_t nodesStart, const size_t nodesEnd);

  /**
   * @brief Hilbert values are used to sort the leafs entries, after this
   * they are not used any more, so is better to delete them.
   */
  void removeHilbertValues(std::vector<node::LeafEntry> &leafs);
  void thr_removeHilbertValues(std::vector<node::LeafEntry> &leafs,
                               const size_t start, const size_t end);

  std::shared_ptr<node::Node>
  bottomUpBuild(std::vector<std::shared_ptr<node::Node>> &leafNodes,
                const std::vector<size_t> &nodeLvlsCnt);

  void thr_bottomUpBuild(std::vector<std::shared_ptr<node::Node>> &parentNodes,
                         std::vector<std::shared_ptr<node::Node>> &childNodes,
                         const size_t childStart, const size_t childEnd,
                         const size_t parentStart, const size_t parentEnd);

public:
  HRtree() = default;
  std::shared_ptr<node::Node> getRoot() const;

  Point *search(const Point &point) const;

  /**
   * @brief Builds the tree using the points passed as parameters.
   */
  bool build(const std::vector<Point> &points);
  // TODO: make a build that move  the points
  // bool build(Point&& point);
  // bool bottomUp(...) ??
};
} // namespace tree
