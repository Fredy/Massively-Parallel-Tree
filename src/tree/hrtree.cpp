#include "hrtree.hpp"
#include "mapper/hilbertMapper.hpp"
#include "node/leafEntry.hpp"
#include "node/node.hpp"
#include "node/nodeEntry.hpp"
#include "node/nonLeafEntry.hpp"
#include "other/config.hpp"
#include "tbb/parallel_sort.h"
#include <algorithm>
#include <thread>
#include <vector>
#include <cassert>

namespace tree {

std::shared_ptr<node::Node> HRtree::getRoot() const { return this->root; }

Point *HRtree::search(const Point &point) const { return nullptr; }

bool HRtree::build(const std::vector<Point> &points) {
  /* 1. Create leaf entries with the points and assign index.
   * 2. Assign hilbert value to the leafs entries.
   * 3. Sort entries by their hilbertValues.
   * 4. Delete their hilbert values?? Check if these aren't used any more.
   * 5. BottomUP
   * */
  if (points.empty())
    return false;

  assert(config::PointDimension() == points.at(0).size()
             && "Dimension specified at config is not the same as the dataset points dimension.");

  // 1. Create leaf entries with the points and assign index.
  std::vector<node::LeafEntry> leafEntries = this->createLeafEntries(points);

  // 2. Assign hilbert value to the leafs entries.
  this->assignLeafsHilbertVal(leafEntries);

  // 3. Sort entries by their hilbertValues.
  tbb::parallel_sort(leafEntries.begin(), leafEntries.end(),
                     [](const node::LeafEntry &a, const node::LeafEntry &b) {
                       return *(a.getHilbertValue()) < *(b.getHilbertValue());
                     });

  // 4. Delete their hilbert values?? Check if these aren't used any more.
  this->removeHilbertValues(leafEntries);

  return true;
}

std::vector<node::LeafEntry>
HRtree::createLeafEntries(const std::vector<Point> &points) {

  size_t numberThreads = std::thread::hardware_concurrency();
  size_t pointsSize = points.size();
  size_t pointsByThread =
      pointsSize / numberThreads + (pointsSize % numberThreads ? 1 : 0);

  std::vector<node::LeafEntry> leafs(pointsSize);

  std::vector<std::thread> threads;
  size_t start = 0;
  size_t end;
  for (size_t i = 0; i < numberThreads; i++) {
    end = std::min(start + pointsByThread, pointsSize);

    threads.emplace_back(&HRtree::thr_createLeafEntries, this, std::ref(points),
                         std::ref(leafs), start, end);

    start = end;
  }

  for (auto &thread : threads)
    thread.join();

  return leafs;
}
void HRtree::thr_createLeafEntries(const std::vector<Point> &points,
                                   std::vector<node::LeafEntry> &leafs,
                                   const size_t start, const size_t end) {
  for (size_t i = start; i < end; i++) {
    leafs[i].setPoint(points[i]);
    leafs[i].setIndex(i);
  }
}
/**
 * @brief Computes the hilbert value of the point of each leaf entry.
 */
void HRtree::assignLeafsHilbertVal(std::vector<node::LeafEntry> &leafs) {
  /* 1. Convert the points of leafEntry in ULLPoints
   * 2. Transform those ULLPoints into their Hilbert Values.
   * 3. Assing the Hilbert Value.
   * */

  // bit precision = 40
  const size_t leafsNum = leafs.size();
  const size_t dimensions = leafs.at(0).getPoint().size();

  size_t numberThreads = std::thread::hardware_concurrency();
  size_t leafsByThread =
      leafsNum / numberThreads + (leafsNum % numberThreads ? 1 : 0);

  std::vector<std::thread> threads;
  size_t start = 0;
  size_t end;
  for (size_t i = 0; i < numberThreads; i++) {
    end = std::min(start + leafsByThread, leafsNum);

    threads.emplace_back(&HRtree::thr_assignLeafHilbertVal, this,
                         std::ref(leafs), dimensions, start, end);

    start = end;
  }

  for (auto &thread : threads)
    thread.join();
}

void HRtree::thr_assignLeafHilbertVal(std::vector<node::LeafEntry> &leafs,
                                      const size_t dimensions,
                                      const size_t start, const size_t end) {

  for (size_t i = start; i < end; i++) {
    PointULL tmpPointULL = pointToPointULL(leafs[i].getPoint(), dimensions);
    leafs[i].setHilbertValue(
        std::make_shared<HilbertValue>(mapper::toHilbertValue(
            tmpPointULL, dimensions, config::HilbertValPrecision())));
  }
}

PointULL HRtree::pointToPointULL(const Point &point, const size_t dimensions) {
  // multC = 1e6, 1e5 is necessary, but we don't use more than 40 bits with this
  // sumC = 1e11, 1e10 is necessary, but we don't use more tan 40 bits with this
  const double multC = 1e6;
  const size_t sumC = size_t(1e11);

  PointULL tmpPointULL(dimensions);
  for (size_t i = 0; i < dimensions; i++) {
    tmpPointULL[i] = size_t(point[i] * multC) + sumC;
  }
  return tmpPointULL;
}

void HRtree::removeHilbertValues(std::vector<node::LeafEntry> &leafs) {

  const size_t leafsNum = leafs.size();
  size_t numberThreads = std::thread::hardware_concurrency();
  size_t leafsByThread =
      leafsNum / numberThreads + (leafsNum % numberThreads ? 1 : 0);

  std::vector<std::thread> threads;
  size_t start = 0;
  size_t end;
  for (size_t i = 0; i < numberThreads; i++) {
    end = std::min(start + leafsByThread, leafsNum);

    threads.emplace_back(&HRtree::thr_removeHilbertValues, this,
                         std::ref(leafs), start, end);

    start = end;
  }

  for (auto &thread : threads)
    thread.join();
}

void HRtree::thr_removeHilbertValues(std::vector<node::LeafEntry> &leafs,
                                     const size_t start, const size_t end) {
  for (size_t i = start; i < end; i++)
    leafs[i].deleteHilbertValue();
}

std::vector<size_t> HRtree::calcNodeLevels(size_t numPoints) {
  std::vector<size_t> nodeLevels;
  while (numPoints > 1) {
    size_t tmp;
    if (numPoints % config::MaxLeafEntries())
      tmp = 1;
    else
      tmp = 0;
    tmp += numPoints / config::MaxLeafEntries();
    nodeLevels.emplace(nodeLevels.begin(), tmp);
    numPoints = tmp;
  }

  return nodeLevels;
}

/**
 * @brief Build the tree in a bottom up way.
 */
void HRtree::bottomUp(std::vector<node::LeafEntry> &leafs) {
  std::vector<size_t> nodeLvlsCnt = this->calcNodeLevels(leafs.size());
  size_t totalNodeCnt = 0;
  for (const auto &n : nodeLvlsCnt)
    totalNodeCnt += n;
  size_t leafNodeCnt = nodeLvlsCnt.back();

  // 1. Assign the leaf entries to their respective nodes
  std::vector<std::shared_ptr<node::Node>> leafNodes =
      this->moveLeafEntriesToNode(leafs, leafNodeCnt);
  // 2. Assign nodes to their parents and calculate their MBR.

  this->root = this->bottomUpBuild(leafNodes, nodeLvlsCnt);
}

/**
 * @brief Assing leafs to their respective nodes.
 */
std::vector<std::shared_ptr<node::Node>>
HRtree::moveLeafEntriesToNode(std::vector<node::LeafEntry> &leafs,
                              const size_t leafCnt) {
  // Construct leafCnt Nodes all are leaf nodes (leaf = true);
  std::vector<std::shared_ptr<node::Node>> nodes(leafCnt);
  for (auto &i : nodes)
    i = std::make_shared<node::Node>(node::Node(true));

  // N-Nodes
  // N-NodesElem
  // N-entries
  // Each thread should have: N-Nodes / N-Threads + (N-Nodes % N-Threads == 0);
  size_t numberThreads = std::thread::hardware_concurrency();
  size_t nodesByThread =
      leafCnt / numberThreads + (leafCnt % numberThreads ? 1 : 0);
  size_t entriesByThread = nodesByThread * config::MaxLeafEntries();

  std::vector<std::thread> threads;
  size_t leafsStart = 0;
  size_t nodesStart = 0;
  size_t leafsEnd, nodesEnd;
  for (size_t i = 0; i < numberThreads; i++) {
    leafsEnd = std::min(leafsStart + entriesByThread, leafs.size());
    nodesEnd = std::min(nodesStart + nodesByThread, leafCnt);

    threads.emplace_back(&HRtree::thr_moveLeafEntriesToNode, this,
                         std::ref(leafs), std::ref(nodes), leafsStart, leafsEnd,
                         nodesStart, nodesEnd);

    leafsStart = leafsEnd;
    nodesStart = nodesEnd;
  }

  for (auto &thread : threads)
    thread.join();
  return nodes;
}

/**
 * @brief Thread function for moveLeafEntriesToNode, ranges: [start, end).
 */
void HRtree::thr_moveLeafEntriesToNode(
    std::vector<node::LeafEntry> &leafs,
    std::vector<std::shared_ptr<node::Node>> &nodes, const size_t leafsStart,
    const size_t leafsEnd, const size_t nodesStart, const size_t nodesEnd) {
  size_t begin = leafsStart;
  size_t end;
  for (size_t i = nodesStart; i < nodesEnd; i++) {
    end = std::min(begin + config::MaxLeafEntries(), leafsEnd);
    std::vector<std::shared_ptr<node::NodeEntry>> leafsPtr(end - begin);

    // Create LeafEntry pointers of the leafs
    for (size_t j = begin; j < end; j++) {
      leafsPtr[j - begin] =
          //    std::make_shared<node::LeafEntry>(leafs[j]);
          std::make_shared<node::LeafEntry>(std::move(leafs[j]));
    }

    // Assing LeafEntry pointer to their node
    nodes[i]->setEntries(std::move(leafsPtr));
    // nodes[i].setEntries(leafsPtr);

    begin = end;
  }
}

std::shared_ptr<node::Node>
HRtree::bottomUpBuild(std::vector<std::shared_ptr<node::Node>> &leafNodes,
                      const std::vector<size_t> &nodeLvlsCnt) {
  size_t treeHeight = nodeLvlsCnt.size();
  std::vector<std::shared_ptr<node::Node>> resNodes = std::move(leafNodes);

  size_t numberThreads = std::thread::hardware_concurrency();

  for (size_t i = treeHeight - 1; i > 1; i--) {
    size_t parentNodes = nodeLvlsCnt[i - 1];
    size_t childNodes = nodeLvlsCnt[i];
    size_t parentsByThread =
        parentNodes / numberThreads + (childNodes % numberThreads ? 1 : 0);
    size_t childsByThread = parentsByThread * config::MaxLeafEntries();

    // Create parent nodes, and make it non leaf nodes (Node(false)).
    std::vector<std::shared_ptr<node::Node>> tmpParentNd(parentNodes);
    for (auto &pNode : tmpParentNd)
      pNode = std::make_shared<node::Node>(node::Node(false));

    // Thread things.
    std::vector<std::thread> threads;
    size_t parentStart = 0;
    size_t childStart = 0;
    size_t parentEnd, childEnd;
    for (size_t j = 0; j < numberThreads; j++) {
      parentEnd = std::min(parentStart + parentsByThread, parentNodes);
      childEnd = std::min(childStart + childsByThread, childNodes);

      threads.emplace_back(&HRtree::thr_bottomUpBuild, this,
                           std::ref(tmpParentNd), std::ref(resNodes),
                           childStart, childEnd, parentStart, parentEnd);

      parentStart = parentEnd;
      childStart = childEnd;
    }

    for (auto &thread : threads)
      thread.join();
    resNodes = std::move(tmpParentNd);
  }
  assert(resNodes.size() <= config::MaxLeafEntries());

  // Assigning nodes to the root.
  std::vector<std::shared_ptr<node::NodeEntry>> rootEntries(resNodes.size());

  // Assign  nodes to their entries in rootEntries.
  for (size_t j = 0; j < rootEntries.size(); j++) {
    rootEntries[j] =
        std::make_shared<node::NonLeafEntry>(std::move(resNodes[j]));
  }

  std::shared_ptr<node::Node> ret =
      std::make_shared<node::Node>(false, std::move(rootEntries));

  return ret;
}

void HRtree::thr_bottomUpBuild(
    std::vector<std::shared_ptr<node::Node>> &parentNodes,
    std::vector<std::shared_ptr<node::Node>> &childNodes,
    const size_t childStart, const size_t childEnd, const size_t parentStart,
    const size_t parentEnd) {
  size_t begin = childStart;
  size_t end;

  for (size_t i = parentStart; i < parentEnd; i++) {
    end = std::min(begin + config::MaxLeafEntries(), childEnd);

    std::vector<std::shared_ptr<node::NodeEntry>> childEntries(end - begin);

    // Assign child nodes to their entries.
    for (size_t j = begin; j < end; j++) {
      childEntries[j - begin] =
          std::make_shared<node::NonLeafEntry>(std::move(childNodes[j]));
    }

    parentNodes[i]->setEntries(std::move(childEntries));
    begin = end;
  }
}

} // namespace tree
